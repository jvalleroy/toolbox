# Emacs customization for FreedomBox

The file `freedombox.el` contains several interactive elisp functions that can
help with FreedomBox development workflow. You can bind them to shortcuts of
your choice. An example for Doom Emacs is provided in `freedombox-doom-shortcuts.el`.

## Installation for Doom Emacs

1. Symlink the two elisp files to ~/.doom.d/
2. Load the two files into your Doom Emacs configuration file by adding the
   following two lines in ~/.doom.d/config.el
   ```elisp
   (load! "freedombox")
   (load! "freedombox-doom-shortcuts")

   ```

After installing the elisp files, all interactive functions will be available
under the group `SPC F`

![](doom-emacs-shortcuts.png)


## Troubleshooting

If your default browser is set to the emacs' built-in `eww` browser, you can
change it by adding the following line to your personal emacs configuration.

``` elisp
(setq browse-url-browser-function 'browse-url-firefox)
```

