;;; ~/.doom.d/freedombox.el
;;; -*- lexical-binding: t; -*-

;; This file is part of FreedomBox.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Recognize django templates with the correct web-mode engine
(defun plinth/django ()
  (if (projectile-project-p)
      (if (file-exists-p (concat (projectile-project-root) "plinth.config"))
          (web-mode-set-engine "django")
        (message "does not exist"))))

(add-hook 'web-mode-hook 'plinth/django)

;; Set all .inc files to be web mode.
;; The function plinth/django already sets engine to django
(add-to-list 'auto-mode-alist '("\\.inc\\'" . web-mode))

;; Utilities to help with FreedomBox development

;; These variables are preferences
;; Use setq in your personal emacs configuration to override defaults
(defvar freedombox-browser "firefox")
(defvar freedombox-messaging-app "quassel")

(defun freedombox-forum()
  "Open FreedomBox forums."
  (interactive)
  (browse-url "https://discuss.freedombox.org"))

(defun freedombox-kanban-board()
  "Open kanban boards of Plinth."
  (interactive)
  (browse-url "https://salsa.debian.org/freedombox-team/freedombox/-/boards"))

(defun freedombox-project-activity()
  "Open activity page of freedombox-team."
  (interactive)
  (browse-url "https://salsa.debian.org/groups/freedombox-team/-/activity"))

(defun freedombox-project-freedombox()
  "Open freedombox project page."
  (interactive)
  (browse-url "https://salsa.debian.org/freedombox-team/freedombox"))

(defun freedombox-list-merge-requests()
  "Open FreedomBox merge requests page."
  (interactive)
  (browse-url "https://salsa.debian.org/groups/freedombox-team/-/merge_requests"))

(defun freedombox-list-issues()
  "Open FreedomBox issues page."
  (interactive)
  (browse-url "https://salsa.debian.org/groups/freedombox-team/-/issues"))

(defun freedombox-goto-issue(number)
  "Open FreedomBox issue by NUMBER."
  (interactive "sIssue number: ")
  (browse-url (concat "https://salsa.debian.org/freedombox-team/freedombox/-/issues/" number)))

(defun freedombox-new-issue-freedombox()
  "Open Salsa to raise a new issue in freedombox."
  (interactive)
  (browse-url "https://salsa.debian.org/freedombox-team/freedombox/issues/new"))

(defun freedombox-chat()
  "Launch or switch to the configured messaging app for FreedomBox."
  (interactive)
  (if (= 1 (shell-command (concat "wmctrl -a " freedombox-messaging-app)))
      (if (string-equal "quassel" freedombox-messaging-app)
          (call-process "quasselclient" nil 0 nil)
        (call-process freedombox-messaging-app nil 0 nil))
    ()))

(defun freedombox-translation()
  "Go to the translation app for FreedomBox projects."
  (interactive)
  (browse-url "https://hosted.weblate.org/projects/freedombox/"))

(defun freedombox-website()
  "Go to freedombox.org"
  (interactive)
  (browse-url "https://freedombox.org"))

(defun freedombox-foundation-news()
  "Go to News section on FreedomBox Foundation website."
  (interactive)
  (browse-url "https://freedomboxfoundation.org/news/"))

(defun freedombox-mastodon-toots()
  "View Mastodon toots from FreedomBox foundation."
  (interactive)
  (browse-url "https://mastodon.social/@freedomboxfndn/"))

(defun freedombox-videos()
  "Go to FreedomBox video channel on PeerTube."
  (interactive)
  (browse-url "https://peertube.mastodon.host/accounts/freedombox/video-channels"))

(defun freedombox-voice-calls()
  "Launch or switch to Mumble for voice calls."
  (interactive)
  (if (= 1 (shell-command (concat "wmctrl -a mumble")))
      (call-process "mumble" nil 0 nil)
    ()))

(defun freedombox-manual-page(page)
  "Refer to the given manual PAGE."
  (interactive "sName of manual page: ")
  (browse-url (concat "https://wiki.debian.org/FreedomBox/Manual/" page)))

(defun freedombox-developer-documentation()
  "Open FreedomBox developer documentation."
  (interactive)
  (browse-url "https://docs.freedombox.org/"))

(defun freedombox-apply-merge-request(mr-number)
  "Apply the merge request specified by MR-NUMBER to the current project."
  (interactive "sMerge request number: ")
  ; apply-mr function must be on PATH
  (call-process "apply-mr" nil nil nil mr-number (projectile-project-name)))

(provide 'freedombox)
;;; freedombox.el ends here
