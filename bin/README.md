# Shell Utilities

To make the utilities in this directory available in your shell, add the
following statement to your .${shell}rc file.

```
export PATH="$PATH:/path/to/freedombox/toolbox/bin"
```

